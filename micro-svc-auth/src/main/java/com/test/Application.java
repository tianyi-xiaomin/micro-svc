package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement // 开启启注解事务管理
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
